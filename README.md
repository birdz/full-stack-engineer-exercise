## What you need to know first:
**Welcome!** This is a small exercise that aims to demonstrate your skills in:

 - Javascript 
 - Node.js
 - AngularJS 
 - HTML5 and CSS3 
 - Relational databases 
 - Web Services
 - Offline communication (docs, commits messages, etc.)

It is important that the code is maintainable and well tested using your preferred testing framework. The goal is to automatically validate that the application does what is asked for in the requirements.

## What you're supposed to do:

We want to develop an internal web application for Fiestify employees.

 - **Part 1**: you have to develop a frontend in AngularJS 1.6 with a CSV import option (design is up to you). This frontend must list the CSV file on screen, and it must also communicate with a backend developed in Node.js that is detailed in the second part.
 - **Part 2**: the frontend must interact with a REST API that allows the upload of CSV files for importing certain data into the main database. To do this, the backend in Node.js (v6 or higher) must be able to receive the CSV file, process it and insert the data into a relational database (PostgreSQL is prefered, but could be any SQL database).

## Instructions & How you should submit your exercise:

 1. Clone the Git repository
 2. Work on the exercise
 3. When you finish, along with the code, add the changes to the README.md with the necessary instructions to run the application. 
 4. Make a **Merge Request** with the changes you want to submit when you finish your work.

Thanks for your time!


## Pasos a seguir


- Instalar Nodejs
- Instalar PostgreSQL
	- Poner contraseña: 123qwe
	- En el caso de tener ya instalado se puede modificar la contraseña en ./fiestify/database/databaseManager.js
- Ejecutar el fichero init-db.sql con postgreSQL
- Instalar Jasmine-node de manera global


### Para ejecutar el programa

- Arrancar el servidor
	- Acceder por consola a la carpeta ./fiestify/
	- Ejecutar "npm start"
- Abrir en el navegador el fichero ./fiestify/view/index.html


### Para ejecutar el test

- Ejecutar "jasmine-node spec"

## Observaciones


El código está muy simplificado para mostrar conocimientos en lo que se pide en el ejercicio. Por ejemplo:


- En el Frontend se podría haber añadido un check para comprobar que el archivo tiene extensión CSV y contiene la estructura deseada
- El testing es muy simple para pasar el ejercicio
- La clase *databaseManager* se podría haber desarrollado mucho más, incluso utilizar herencia para poder cambiar de tecnología fácilmente, haber utilizado una librería para construir las querys y utilizar beans para cada tipo de dato donde gestionar y comprobar los datos que se vayan a introducir o se obtengan de la base de datos.


