var promise = require('bluebird'); 

var initOptions = {
    promiseLib: promise 
};

var pgp = require('pg-promise')(initOptions);
var QueryManager = require('./queryManager');
var queryManager = new QueryManager();
var databaseManager = null;
function DatabaseManager () {

	var db = pgp({
	    host: 'localhost',
	    port: 5433,
	    database: 'fiestifyDb',
	    user: 'postgres',
	    password: '123qwe'
	});

	this.insert = function(data1, data2, data3) {
		return one(queryManager.getInsertQuery([data1, data2, data3]));
	}
	this.getAll = function() {
		return any(queryManager.getSelectQuery());
	}
	this.getById = function(id) {
		return any(queryManager.getSelectQuery(id));
	}
	this.update = function(id, data1, data2, data3) {
		return one(queryManager.getUpdateQuery(id, [data1, data2, data3]));
	}
	this.remove = function(id) {
		return one(queryManager.getDeleteQuery(id));	
	}

	var one = function(query) {
		return db.one(query.query, query.data);
	}
	var any = function(query) {
		return db.any(query.query, query.data);
	}
}
var getDatabaseManager = function() {
	if(!databaseManager) {
		databaseManager = new DatabaseManager();
	}
	return databaseManager;
}
module.exports.getDatabaseManager = getDatabaseManager;