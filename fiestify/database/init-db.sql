DROP DATABASE IF EXISTS fiestifyDb;
CREATE DATABASE fiestifyDb;

\c fiestifyDb;

CREATE TABLE fiestifyTable (
  ID SERIAL PRIMARY KEY,
  text1 VARCHAR,
  text2 VARCHAR,
  text3 VARCHAR
);