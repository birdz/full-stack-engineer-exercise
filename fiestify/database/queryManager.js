function QueryManager () {
	var _this = this;
	_this.getSelectQuery = function(id) {
		if(id) {
			return {
				query : 'SELECT * FROM fiestifytable WHERE id=($1)',
				data: [id]
			}		
		} else {
			return {
				query : 'SELECT * FROM fiestifytable ORDER BY id ASC',
				data: null
			}		
		}
	}
	_this.getDeleteQuery = function(id) {
		return {
				query : 'DELETE FROM fiestifytable WHERE id=($1) RETURNING id',
				data: [id]
			}
	}
	_this.getInsertQuery = function(data) {
		var obj = {
			text1: data[0],
			text2: data[1],
			text3: data[2]
		}
		return {
				query : 'INSERT INTO fiestifytable(${this:name}) values(${this:csv}) RETURNING id',
				data: obj
			}
	}
	_this.getUpdateQuery = function(id, data) {
		console.log('DB UPDATE', data);
		data.push(id);
		console.log('DB UPDATE', data);
		return {
				query : 'UPDATE fiestifytable SET text1=($1), text2=($2), text3=($3) WHERE id=($4) RETURNING id',
				data: data
			}
	}
}

module.exports = QueryManager;