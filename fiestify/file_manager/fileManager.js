var multer  = require('multer');
var fs = require('fs');
var path = require('path');


function FileManager () {

	var storage = multer.diskStorage({
	  destination: './uploads/',
	  filename: function (req, file, cb) {
	    cb(null, file.originalname.replace(path.extname(file.originalname), "") + '-' +  Date.now() + path.extname(file.originalname));
	  }
	});

	this.upload = multer({ storage: storage });

	this.fileReader = function(file, callback){
	    if (file) {
			try {
				fs.readFile(file.path, 'utf8', callback);
			} catch(e) {
				console.log(e);
			}  
	    }
	}
	this.fileDecode = function(fileContent) {
		var rows = fileContent.split('\n');
		for(var i = 0, count = rows.length; i < count; i++) {
			rows[i] = rows[i].split(',');
		}
		return rows;

	}

}
module.exports = FileManager;