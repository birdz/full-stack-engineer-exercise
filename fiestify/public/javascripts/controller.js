var app = angular.module("app", [])

.controller('HomeCtrl', ['$scope', 'upload', function ($scope, upload) 
{
	$scope.uploadFile = function()
	{
		var name = $scope.file.name;
		var file = $scope.file;
		upload.uploadFile(file, name)
	}
}])

.directive('uploaderModel', ["$parse", function ($parse) {
	return {
		restrict: 'A',
		link: function (scope, iElement, iAttrs) 
		{
			iElement.on("change", function(e)
			{
				$parse(iAttrs.uploaderModel).assign(scope, iElement[0].files[0]);
				var file = iElement[0].files[0];
		        if (file) {
		          var r = new FileReader();
		          r.onload = function(e) {
		              var contents = e.target.result;
		              scope.$apply(function () {
		                scope.fileContent = contents;
		              });
		          };
		          
		          r.readAsText(file);
		        }
			});
		}
	};
}])
.service('upload', ["$http", "$q", function ($http, $q) 
{
	this.uploadFile = function(file, name)
	{
		console.log('UPLOAD', file, name);
		var deferred = $q.defer();
		var formData = new FormData();
		formData.append("name", name);
		formData.append("file", file);
		$http.post("/upload", formData, {
			headers: {
				"Content-type": undefined
			},
			transformRequest: angular.identity
		})
		.then(function(res)
		{
			console.log('SUCCES', res.data);
			deferred.resolve(res);
		});
	}	
}])