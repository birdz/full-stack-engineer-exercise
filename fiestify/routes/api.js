var express = require('express');
var router = express.Router();
var db = require('../database/databaseManager').getDatabaseManager();
var FileManager = require('../file_manager/fileManager');
var fileManager = new FileManager();


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});
router.post('/upload', fileManager.upload.single('file'), function(req, res){
	fileManager.fileReader(req.file, callbackFileManager(res))
});

var callbackFileManager = function(res) {
	return function(err, contents) {
		var arrayData = fileManager.fileDecode(contents);
		for(var i = 0, count = arrayData.length; i < count; i++) {
			db.insert(arrayData[i][0], arrayData[i][1], arrayData[i][2]).then(function (data) {
				res.send(data);
			});
		}
		
	}
}
module.exports = router;
