 var db = require('../database/databaseManager').getDatabaseManager();
describe("Pruebas CRUD base de datos", function() {

    beforeEach(function() {
       
    });

    it("debe insertar un elemento", function(done) {
        db.insert("uno", "dos", "tres").then(function(result) {
            expect(Number.isInteger(result.id)).toBe(true);
            done();
        }); 
    });

    it("debe obtener todos los elementos", function(done) {
        db.getAll().then(function(result){
            expect(result.length >= 0).toBe(true);
            done();
        });
    });

    it("debe obtener un elemento a partir de su id", function(done) {
        db.getById(5).then(function(result){
            expect(true).toBe(true);
            done();
        });
    });

    it("debe actualizar un elemento a partir de su id", function(done) {
        db.update(8, "uno", "dos", "tres").then(function(result){
            expect(Number.isInteger(result.id)).toBe(true);
            done();
        });
    });

    it("debe eliminar un elemento a partir de su id", function(done) {
        db.remove(4).then(function(result){
            expect(Number.isInteger(result.id)).toBe(true);
            done();
        }).catch(function(error){
            expect(error.received == 0).toBe(true);
            done();
        });
    });
});